const response = [
  {
    _id: "61398b4a84c0ba327c4f8565",
    title: "Articulo 2",
    price: "4300",
    thumbnail: "https://opencollective.com/pug/sponsor/3/avatar.svg",
    __v: 0,
  },
  {
    _id: "613fe5f935ce6ab354b66413",
    title: "Articulo 3",
    price: "45",
    thumbnail:
      "https://cdn0.iconfinder.com/data/icons/multimedia-solid-30px/30/shuffle_mix_random_arrow-256.png",
    __v: 0,
  },
];

module.exports = response;
