module.exports = class {
  constructor(data) {
    this.data = data;
  }

  find = function () {
    const { data } = this;
    return new Promise(function (resolve, reject) {
      if (data) {
        resolve(data);
      } else {
        reject(new Error("Fake error on find"));
      }
    });
  };
  findById = function (id) {
    const { data } = this;
    return new Promise(function (resolve, reject) {
      if (data) {
        if (id === "validId") {
          resolve(data);
        } else resolve(null);
      } else {
        reject(new Error("Fake error on findById"));
      }
    });
  };

  findByIdAndUpdate = function (id, payload) {
    const { data } = this;
    return new Promise(function (resolve, reject) {
      if (data) {
        if (id === "validId") {
          resolve(data);
        } else resolve(null);
      } else {
        reject(new Error("Fake error on findById"));
      }
    });
  };

  findByIdAndDelete = function (id) {
    const { data } = this;
    return new Promise(function (resolve, reject) {
      if (data) {
        if (id === "validId") {
          resolve(data);
        } else resolve(null);
      } else {
        reject(new Error("Fake error on findById"));
      }
    });
  };

  create = function (product) {
    const { data } = this;
    return new Promise(function (resolve, reject) {
      if (data) resolve(data);
      else reject(new Error("fake error on create"));
    });
  };
};
