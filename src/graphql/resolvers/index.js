const Product = require("../../dao/models/product");

module.exports = {
  products: async () => {
    try {
      const productsFetched = await Product.find();
      return productsFetched;
    } catch (error) {
      throw error;
    }
  },

  product: async (args) => {
    const { id } = args;
    try {
      const productFetched = await Product.findById(id);
      return productFetched;
    } catch (error) {
      throw error;
    }
  },

  createProduct: async (args) => {
    try {
      console.log("probando desde graphql");
      console.log(args);
      const { title, price, thumbnail } = args.product;
      const productCreated = await Product.create({ title, price, thumbnail });
      return productCreated;
    } catch (error) {
      throw error;
    }
  },
};
