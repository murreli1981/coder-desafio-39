# coder-desafio-39

Implementación de unit test con jest

## Estructura del proyecto:

```
.
├── __test__
│   ├── __mock__
│   ├── services
│   └── utils
├── coverage
│   └── lcov-report
│       ├── __test__
│       │   ├── __mock__
│       │   └── utils
│       └── src
│           ├── config
│           ├── dao
│           │   └── models
│           ├── services
│           └── utils
├── imgs
├── logs
├── performance
│   └── report
├── src
│   ├── auth
│   ├── config
│   ├── controllers
│   ├── dao
│   │   ├── db
│   │   └── models
│   ├── graphql
│   │   ├── resolvers
│   │   └── schema
│   ├── resources
│   ├── routes
│   ├── services
│   ├── utils
│   └── views
│       ├── layouts
│       └── partials
└── target
```

## Inicio del server

nodemon

npm start

### url:

`http://localhost:8080/ingreso`

### test y coverage

los test se corren con el comando

`npm test`

que referencia a la siguiente tarea en el package.json

`"test": "jest --coverage"`

```
 PASS  __test__/services/product.test.js
  service product unit test
    ✓ getAllProducts(): it should return an array of elements when they exist (5 ms)
    ✓ getAllProducts(): the product returned should contain id, item, price and thumbnail (1 ms)
    ✓ getAllProducts() it should return an empty array when there's no result (3 ms)
    ✓ getProduct() it should return null when a given product is not found (1 ms)
    ✓ getProduct() it should return null when a non-mongo id kind is being sent
    ✓ getProduct() it should return an item with a valid id (4 ms)
    ✓ addProduct() it should return the add information when it's added (3 ms)
    ✓ updateProduct() it should return a valid object after updating (6 ms)
    ✓ deleteProduct() it should return a valid response after deleting a valid object (43 ms)
    ✓ should return undefined when model fails (63 ms)



----------------------|---------|----------|---------|---------|-------------------
File                  | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
----------------------|---------|----------|---------|---------|-------------------
All files             |   97.93 |    74.19 |     100 |   97.91 |
 __test__/__mock__    |     100 |      100 |     100 |     100 |
  item-found-data.js  |     100 |      100 |     100 |     100 |
  item-stored-data.js |     100 |      100 |     100 |     100 |
  items-found-data.js |     100 |      100 |     100 |     100 |
  no-items-found.js   |     100 |      100 |     100 |     100 |
 __test__/utils       |   94.73 |     87.5 |     100 |   94.59 |
  fake-mongo-model.js |   94.73 |     87.5 |     100 |   94.59 | 35,48
 src/config           |     100 |       60 |     100 |     100 |
  globals.js          |     100 |       60 |     100 |     100 | 8-14
 src/dao/models       |     100 |      100 |     100 |     100 |
  product.js          |     100 |      100 |     100 |     100 |
 src/services         |     100 |      100 |     100 |     100 |
  product.js          |     100 |      100 |     100 |     100 |
 src/utils            |     100 |      100 |     100 |     100 |
  loggers.js          |     100 |      100 |     100 |     100 |
----------------------|---------|----------|---------|---------|-------------------
```

1. `__test__/services/product.test.js`suite de test que validar el servicio product.
2. `__test__/utils/fake-mongo-model.js` mock del model de mongo que se inyecta como dependencia en el product
3. `__test__/__mock__/*-data.js` - mocks de los diferentes tipos de respuesta que espero del model para los unit.
