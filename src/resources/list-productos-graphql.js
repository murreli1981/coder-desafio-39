const tableBody = document.querySelector("#product-tbody");

fetch(`/graphql?query={
  products {
    title
    price
    thumbnail
  }
}`)
  .then((res) => res.json())
  .then(({ data }) => {
    console.log(data);
    data.products.forEach((prod, index) => {
      tableBody.innerHTML += `<tr>
          <td>${index + 1}</td>
          <td><strong>${prod.title}</strong></td>
          <td>${prod.price}</td>
          <td><img src="${prod.thumbnail}" width="30" /></td>
          </tr>`;
    });
  });
