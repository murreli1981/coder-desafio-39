const productModel = require("../dao/models/product");
const logger = require("../utils/loggers");

const log = logger.getLogger("default");
const logFile = logger.getLogger("file");
const ERR_SAVE_MSG = "SERVICE Product: Not able to store product";
const ERR_GET_MSG = "SERVICE Product: Not able to get product";

module.exports = class {
  constructor(model) {
    this.model = model;
  }

  async addProduct(product) {
    try {
      const productCreated = await this.model.create(product);
      return productCreated;
    } catch (e) {
      log.error(ERR_SAVE_MSG);
      logFile.error(`${ERR_SAVE_MSG} product: ${product.nombre} error: ${e}`);
      return null;
    }
  }

  async getProduct(id) {
    try {
      const response = await this.model.findById(id);
      return response;
    } catch (error) {
      log.error(ERR_GET_MSG);
      logFile.error(ERR_GET_MSG + " id: " + id + " error: " + error);
      return null;
    }
  }

  async getAllProducts() {
    try {
      const items = await this.model.find();
      return items;
    } catch (error) {
      log.error(ERR_GET_MSG);
      logFile.error(`${ERR_GET_MSG} error: + ${error}`);
    }
  }

  async updateProduct(id, payload) {
    try {
      const productEdited = await this.model.findByIdAndUpdate(id, payload);
      return productEdited;
    } catch (error) {
      log.error(ERR_GET_MSG);
      logFile.error(ERR_GET_MSG + " id: " + id + " error: " + error);
      return null;
    }
  }

  async deleteProduct(id) {
    try {
      const productDeleted = await this.model.findByIdAndDelete(id);
      console.log(productDeleted);
      return productDeleted;
    } catch (error) {
      log.error(ERR_GET_MSG);
      logFile.error(ERR_GET_MSG + " id: " + id + " error: " + error);
      return null;
    }
  }
};
