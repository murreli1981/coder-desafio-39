const { PORT, MONGO_URI, SECRET_KEY, IS_CLUSTER } = require("./config/globals");
const NotificationService = require("./services/notifications");

//server
const http = require("http");
const express = require("express");
const app = express();
const server = http.createServer(app);
const router = express.Router();
const cookieParser = require("cookie-parser");
const cluster = require("cluster");
const ProcessInfo = require("./services/process-info");
const numCPUs = new ProcessInfo().getNumCPUs();
const compression = require("compression");

//db
const MongoStore = require("connect-mongo");

//authentication
const passport = require("passport");
const { isAuth } = require("./controllers/authentication");
const session = require("express-session");
require("./auth/local");
require("./auth/facebook");

//socket
const { Server } = require("socket.io");
const io = new Server(server);

//graphql
var { graphqlHTTP } = require("express-graphql");
const graphqlSchema = require("./graphql/schema");
const graphqlResolvers = require("./graphql/resolvers");

const mongoOptions = { useNewUrlParser: true, useUnifiedTopology: true };

const sessionMiddleware = session({
  store: MongoStore.create({
    mongoUrl: `${MONGO_URI}`,
    mongoOptions,
  }),
  secret: `${SECRET_KEY}`,
  resave: true,
  saveUninitialized: true,
  rolling: true,
  cookie: {
    maxAge: 20 * 1000 * 60,
  },
});

const productRoutes = require("./routes/product");
const userRoutes = require("./routes/user");
const extrasRoutes = require("./routes/extras");

const { getConnection } = require("./dao/db/connection");

app.set("views", "./src/views");
app.set("view engine", "ejs");

//paso el socket
app.use((req, res, next) => {
  req.io = io;
  //console.log(`[${process.pid}] Path: ${req.path}`);
  next();
});

//agrego todos los middlewares
app.use(
  "/graphql",
  graphqlHTTP({
    schema: graphqlSchema,
    rootValue: graphqlResolvers,
    graphiql: true,
  })
);

app.use(sessionMiddleware);
app.use(express.json());
app.use(express.urlencoded());
app.use(passport.initialize());
app.use(passport.session());
app.use("/public", express.static("./src/resources"));
app.use(productRoutes(router));
app.use(userRoutes(router));
app.use(extrasRoutes(router));
app.use(cookieParser());
app.use(compression());

if (cluster.isMaster && IS_CLUSTER) {
  console.log("*** SERVER RUNNING IN CLUSTER MODE *** ");
  console.log(`[${process.pid}] Parent process`);

  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }
} else {
  app.get("/ingreso", isAuth, (req, res) => {
    req.io = io;
    res.render("input", {
      username: req.user.username,
      fb: req.cookies["fb"] !== "undefined" ? req.cookies["fb"] : null,
      email: req.cookies["email"] !== "undefined" ? req.cookies["email"] : null,
    });
  });

  //inicializo mongo y server
  getConnection().then(() =>
    server.listen(PORT, () => {
      console.log(`[${process.pid}] server is up and running on port ${PORT}`);
    })
  );

  //socket
  io.on("connection", async (socket) => {
    const ProductService = require("./services/product");
    const MessageService = require("./services/message");
    productService = new ProductService();
    messageService = new MessageService();

    console.log("new connection", socket.id);

    io.sockets.emit("list:products", await productService.getAllProducts());
    io.sockets.emit("chat:messages", await messageService.getAllMessages());

    socket.on("chat:new-message", async (data) => {
      const { author, message } = data;
      const entry = {
        author,
        message,
      };
      //valido si existe la palabra administrador en el string del mensaje
      if (message.toLowerCase().trim().includes("administrador")) {
        const { nombre, apellido } = entry.author;
        //llamo al sendSMS del servicio de notificaciones y le paso nombre, apellido y el mensaje entero
        new NotificationService().sendSMS(nombre + " " + apellido, message);
      }
      await messageService.addMessage(entry);
      io.sockets.emit("chat:messages", await messageService.getAllMessages());
    });
  });
}
