const NotificationService = require("../services/notifications");
const passport = require("passport");
const fs = require("fs");
const cookieParser = require("cookie-parser");

exports.registerPage = async (req, res, next) => {
  res.render("register");
};

exports.loginPage = async (req, res, next) => {
  res.render("login");
};

// exports.register = async (req, res, next) => {
//   req.session.isLogged = true;
//   res.redirect("/input");
// };

exports.login = async (req, res, next) => {
  const notificationService = new NotificationService();
  req.session.isLogged = true;
  req.session.username = req.user.username;
  req.session.fb = req.user.fb; //todelete
  req.session.email = req.user.email; //todelete

  //llamo al servicio de notificiones para que mande los mails
  notificationService.sendMail(
    req.user.username,
    "Log-in",
    req.user.email || null,
    req.user.fb || null
  );
  res.cookie("test", 1);
  res.cookie("fb", req.user.fb);
  res.cookie("email", req.user.email);
  res.redirect("/ingreso");
};

exports.loginFailed = async (req, res, next) => {
  res.render("login-failed");
};

exports.registerFailed = async (req, res, next) => {
  res.render("register-failed");
};

exports.logout = async (req, res, next) => {
  req.logout();
  const notificationService = new NotificationService();
  //llamo al servicio de notificaciones para que envío un mail de logout
  notificationService.sendMail(req.session.username, "Log-out");
  res.render("login");
};
